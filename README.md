How to use
==========
Install repo

- Option 1. Install from corporate gitlab repo.

```bash
cd ~
git clone https://gitlab.com/ALU/nuage_vsd_abstraction_from_yaml.git
```

- Option 2. [Download](http://gitlab.us.alcatel-lucent.com/agorbach/nuage-provision-overlay-via-api/repository/archive.zip?ref=master) and unpack archive.

Provision overlay from configuration file

```bash
python nuage_api_example.py -i
```

Provision ACLs from configuration file

```bash
python nuage_api_example.py -a
```

Requierements
=============
- Python 2.7. Can be downloaded [here](https://www.python.org/downloads/release/python-2712/).
- Install package requirements

```bash
$ pip install -r requirements.txt
```

User guide
==========
Repo includes 2 main files:
- Scripts itself 'nuage_api_example.py'
- Configuration file 'config.yaml'

Script supports bash cli arguments

```bash
$python nuage_api_example.py --help
usage: nuage_api_example.py [-h] [-c CONFIG] [-v] [-i] [-a]

Script giving example of Nuage VSD API usage

optional arguments:
  -h, --help            show this help message and exit
  -c CONFIG, --config-file CONFIG
                        File describing network abstraction in yaml format.
                        Default is config.yaml.
  -v, --verbose         Enable verbose output
  -i, --provision-overlay
                        Provision initial network abstraction in VSD
  -a, --provision-acl   Provision ACL according to configuration file
```

Configuration file describes overlay in YAML format

```yaml
# VSD connection parameters
vsd:
  username:     csproot
  password:     csproot
  organization: csp
  host:         172.17.9.129
  port:         8443
# Openstack connection parameters
openstack:
  username:     admin
  password:     Alcateldc
  project:      csp
  host:         172.17.9.129
# Overlay parameters
organization:
  # organizatoin (tenant) name
  name: API
  # organization domains
  domains:
    DOMAIN1:                        # Domain name
      zone1:                        # Zone name inside domain DOMAIN1
        subnet1:                    # Subnet name inside zone zone1
          address: '11.0.0.0'
          netmask: '255.255.255.0'
      zone2:                        # Zone name inside domain DOMAIN1
        subnet2:                    # Subnet name inside zone zone2
          address: '12.0.0.0'
          netmask: '255.255.255.0'
  acls:
    DOMAIN1:                        # Domain name to apply ACL
      ingress:                      # Ingress ACL definition
        default: reject             # Default ACL action
        allow:                      # All allow entries
          entry10:
            from:  any
            to:    any
            protocol: icmp
          entry20:
            from:  subnet1
            to:    subnet2
            protocol: tcp
            dport: 22
            sport: any
        drop:                       # All drop entries
          entry30:
            from:  zone1
            to:    zone2
            protocol: tcp
            dport: 80
            sport: any
      egress:                       # Egress ACL definition
        default: accept
```


More about ACLs
---------------

```yaml
  acls:
    DOMAIN1:
      ingress:
        default: reject
```

if default action is not defined, than accept will be used.

```yaml
  acls:
    DOMAIN1:
      ingress:
        default: reject
        allow:
          entry10:
            from:  any
            to:    any
            proto: icmp
```

If protocol, destination, source or ports are not defined, than any will be used.