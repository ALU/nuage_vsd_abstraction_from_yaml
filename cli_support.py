import argparse
import logging
import reyaml


def get_args():
    '''
    Supports the command-line arguments listed below.
    :return: script command-line arguments
    '''

    parser = argparse.ArgumentParser(description="Script giving example of Nuage VSD API usage")
    parser.add_argument('-c','--config-file', required=False,
                        help='File describing network abstraction in yaml format. Default is config.yaml.',
                        dest='config', type=str)
    parser.add_argument('-l','--log-file', required=False,
                        help='Log file destination. Default is vsd_abstraction_from_yaml.log.',
                        dest='log_file', type=str, default='vsd_abstraction_from_yaml.log')
    parser.add_argument('-v', '--verbose', required=False, help='Enable verbose output',
                        dest='verbose', action='store_true')
    parser.add_argument('-d', '--debug', required=False, help='Enable debug output',
                        dest='debug', action='store_true')
    parser.add_argument('-i', '--provision-overlay', required=False,
                        help='Provision initial network abstraction in VSD',
                        dest='init', action='store_true', default=False)
    parser.add_argument('-a', '--provision-acl', required=False, help='Provision ACL according to configuration file',
                        dest='add_acl', action='store_true', default=False)
    parser.add_argument('--show-config', required=False, help='Show configuration file',
                        dest='show_config', action='store_true', default=False)
    parser.add_argument('--list-org', required=False, help='Show all VSD organizations',
                        dest='list_org', action='store_true', default=False)

    return parser.parse_args()

def get_logger(arguments):
    '''
    Create root logger object
    :param arguments: command line arguments object
    :return: root logger object
    '''
    logger = logging.getLogger('nuage')
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    ch = logging.StreamHandler()
    ch.setFormatter(formatter)
    logger.addHandler(ch)
    if arguments.log_file:
        fh = logging.FileHandler(arguments.log_file)
        fh.setFormatter(formatter)
        logger.addHandler(fh)
    if arguments.verbose:
        logger.setLevel(logging.INFO)
    if arguments.debug:
        logger.setLevel(logging.DEBUG)

    return logger


def load_yaml_config(arguments):
    '''
    Load yaml configuration file. If not specified by user, load default file config.yaml
    :param arguments: command line arguments object
    :return: dictionary from yaml configuration file
    '''

    module_logger = logging.getLogger('nuage.load_yaml_config')
    if arguments.config:
        module_logger.info('Load yaml configuration file {0} from user input.'.format(arguments.config))
        config = reyaml.load_from_file(arguments.config)
    else:
        module_logger.warning(
            'There is no user defined yaml configuration file. Try to load default configuration file config.yaml')
        config = reyaml.load_from_file('config.yaml')
    return config


