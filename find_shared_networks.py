from vspk import v4_0 as vsdk
nc = vsdk.NUVSDSession(username='csproot',
                       password='csproot',
                       enterprise='csp',
                       api_url="https://10.166.72.100:8443")
nc.start()
nuage_user = nc.user

def get_vsd_objects(obj, _path=None):
    '''
    Function recursively search absolute path for VSD object up to the top level
    :param obj: VSD object for which full path is going to be searched
    :param _path: initial path list. should not be changed
    :return: list with full path
    '''
    if _path is None:
        _path = list()
    if obj is not None:
        if obj.parent_id is not None:
            _path.append(obj.name)
            parrent_obj = nuage_user.fetcher_for_rest_name(obj.parent_type).get_first(obj.parent_id)
            return get_vsd_objects(parrent_obj, _path)
        else:
            _path.append(obj.name)
    return list(reversed((_path[:])))

for subnet in nuage_user.subnets.get():
    if subnet.associated_shared_network_resource_id is not None:
        shared_net_obj = vsdk.NUSharedNetworkResource(id=subnet.associated_shared_network_resource_id).fetch()[0]
        print('#'*10+'\n"{0}" subnet is associated with shared network "{1}"'.format(subnet.name,
                                                                                     shared_net_obj.name))

        print("This is full path to this subnet:\n{0}".format(' -> '.join(get_vsd_objects(subnet))))
