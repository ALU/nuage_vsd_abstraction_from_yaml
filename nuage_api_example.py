import cli_support
from pprint import pprint, pformat
from vspk import v4_0 as vsdk

def org_already_exists(__org_name):
    '''
    Function checks if organization already exists in VSD.

    :param __org_name: name of organization to check in VSD
    :return: True if organization already exists in VSD.
    '''

    __org = nuage_user.enterprises.get_first(filter=('name == "{0}"'.format(__org_name)))
    if __org is None:
        return False
    else:
        return True


def push_to_vsd(_vsd_obj_type, _NUObject, _parent):
    '''
    Function create child object in VSD and save new object in local variable.
    :param _vsd_obj_type: type of object to identify it in local DB
    :param _NUObject: new object to be created
    :param _parent:  parent object for new object
    :return:
    '''
    _obj = _parent.create_child(_NUObject)[0]
    vsd_obj.update({_vsd_obj_type: _obj})
    logger.debug(pformat(_obj.to_dict()))


def deploy_org_networks():
    '''
    Deploy all network elements, except ACLs, from configuration yaml file
    :return: None
    '''
    push_to_vsd('organization',
                vsdk.NUEnterprise(data={'name': config['organization']['name']}),
                nuage_user)
    push_to_vsd('domain_template',
                vsdk.NUDomainTemplate(name='TEMPLATE'),
                vsd_obj.get('organization'))
    for domain_name, domain_dict in sorted(config['organization']['domains'].items()):
        logger.info('Provision domain with name {0}'.format(domain_name))
        push_to_vsd(domain_name,
                    vsdk.NUDomain(data={'name': domain_name,
                                        'templateID': vsd_obj.get('domain_template').to_dict()['ID']}),
                    vsd_obj.get('organization'))
        for zone_name, zone_dict in domain_dict.items():
            logger.info('Provision zone with name {0}'.format(zone_name))
            push_to_vsd('{0}_{1}'.format(domain_name, zone_name),
                        vsdk.NUZone(name=zone_name),
                        vsd_obj.get(domain_name))
            for subnet_name, subnet_dict in zone_dict.items():
                sub_address = subnet_dict['address']
                sub_netmask = subnet_dict['netmask']
                logger.info('Provisioning subnet with name{0} ({1}/{2})'.format(subnet_name,
                                                                                sub_address,
                                                                                sub_netmask))
                push_to_vsd('{0}_{1}_{2}'.format(domain_name, zone_name, subnet_name),
                            vsdk.NUSubnet(data={'name': subnet_name,
                                                'address': sub_address,
                                                'netmask': sub_netmask}),
                            vsd_obj.get('{0}_{1}'.format(domain_name, zone_name)))


def get_id_from_nuage_obj():
    '''
    Conveert Nuage VSD objects dictionary to dictionary with VSD objects and IDs only
    :return: dictionary with VSD objects IDs
    '''
    __tmp = {}
    for name, obj in vsd_obj.items():
        __tmp.update({name: str(obj.to_dict()["ID"])})
    return __tmp


def build_vsd_obj_dict():
    __org = nuage_user.enterprises.get_first(filter=('name == "{0}"'.format(config['organization']['name'])))
    if __org is None:
        pprint('FAILED: Cannot find organizarion with name {}. Please check configuration'.format(
            config['organization']['name']))
        exit(-1)
    vsd_obj.update({'organization': __org})
    for domain_template in __org.fetcher_for_rest_name('domaintemplate').fetch()[2]:
        if args.debug: pprint(domain_template)
        vsd_obj.update({'domain_template': domain_template})
    for domain in __org.fetcher_for_rest_name('domain').fetch()[2]:
        if args.debug: pprint(domain)
        vsd_obj.update({str(domain.name): domain})
        for zone in domain.fetcher_for_rest_name('zone').fetch()[2]:
            if args.debug: pprint(zone)
            vsd_obj.update({'{0}_{1}'.format(domain.name, zone.name): zone})
            for subnet in zone.fetcher_for_rest_name('subnet').fetch()[2]:
                if args.debug: pprint(subnet)
                vsd_obj.update({'{0}_{1}_{2}'.format(domain.name, zone.name, subnet.name): subnet})


def push_acl_to_vsd(__acl):
    __actions_rename = {'allow': 'FORWARD',
                        'drop': 'DROP'}
    __L4_ports_remame = {'sport': 'sourcePort',
                         'dport': 'destinationPort'}
    __protocol_map = {'tcp': '6',
                      'icmp': '1',
                      'udp': '17'}
    __directions_map = {'from': 'locationType',
                        'to': 'networkType',
                        'from_id': 'locationID',
                        'to_id': 'networkID'}

    def build_entry_type1(entry_params):
        __tmp = {'protocol': __protocol_map[entry_params.get('protocol')]}
        for param in ['dport', 'sport']:
            if entry_params.get(param):
                if str(entry_params.get(param)).lower() == 'any':
                    __tmp.update({__L4_ports_remame[param]: '*'})
                else:
                    __tmp.update({__L4_ports_remame[param]: str(entry_params[param])})
            else:
                print('WARNING. Parameter {} is not defined. "ANY" is going to be used'.format(param))
                __tmp.update({__L4_ports_remame[param]: '*'})
        # pprint(__tmp)
        return __tmp

    def build_entry_type2(entry_params):
        __tmp = {'protocol': __protocol_map[entry_params.get('protocol')]}
        # __tmp = {'protocol': 'ICMP'}
        for param in ['icmptype', 'icmpcode']:
            if entry_params.get(param) and str(entry_params.get(param)).lower() == 'any':
                __tmp.update({param: '*'})
            elif entry_params.get(param):
                __tmp.update({param: str(entry_params[param]).upper()})
            else:
                print('WARNING. Parameter {} is not defined. "ANY" is going to be used'.format(param))
                __tmp.update({param: '*'})
        # pprint(__tmp)
        return __tmp

    def build_entry_direction_data(entry_params):
        def _finditem(obj, key, _path):
            if key in obj:
                return _path + [key]
            for k, v in obj.items():
                if isinstance(v, dict):
                    _path.append(k)
                    return _finditem(v, key, _path)

        def build_path_endpoint(endpoint):
            if entry_params.get(endpoint):
                if entry_params.get(endpoint).lower() == 'any':
                    __tmp.update({__directions_map[endpoint]: 'ANY'})
                else:
                    nuobject_path = _finditem(config['organization']['domains'], entry_params.get(endpoint), [])
                    if nuobject_path is not None:
                        nuobject_path = '_'.join(nuobject_path)
                        # print(nuobject_path)
                        # pprint(vsd_obj[nuobject_path].to_dict())
                        # pprint(vsd_obj[nuobject_path].rest_name.upper())
                        # pprint(vsd_obj[nuobject_path].id)
                        __tmp.update({__directions_map[endpoint]: vsd_obj[nuobject_path].rest_name.upper(),
                                      __directions_map[endpoint + '_id']: vsd_obj[nuobject_path].id})
                    else:
                        print('WARNING. Cannot find target direction defined. "ANY" is going to be used')
                        __tmp.update({__directions_map[endpoint]: 'ANY'})
            else:
                print('WARNING. No target direction defined. "ANY" is going to be used')
                __tmp.update({__directions_map[endpoint]: 'ANY'})

        __tmp = {}
        build_path_endpoint('to')
        build_path_endpoint('from')
        return __tmp

    def default_acction(action, direction):
        common = {'defaultAllowNonIP': action,
                  'defaultAllowIP': action}
        if direction == 'ingress':
            common.update({'allowAddressSpoof': False})
            pprint(common)
            return common
        elif direction == 'egress':
            return common
        else:
            print(direction)
            return common

    def create_entries(action, entries):
        _entries = []
        for entry, entry_params in entries.items():
            # general part of entry
            _entry_data = {'description': entry.upper(),
                           "priority": str(entry.lstrip('entry')),
                           "action": __actions_rename[action]}
            # traffic specific type
            if entry_params.get('protocol') in ['tcp', 'udp']:
                _entry_data.update(build_entry_type1(entry_params))
            elif entry_params.get('protocol') in ['icmp']:
                _entry_data.update(build_entry_type2(entry_params))
            # direction specific
            _entry_data.update(build_entry_direction_data(entry_params))
            # pprint(_entry_data)
            _entries += [_entry_data]
        return _entries

    def create_ingress_acl(__ingress_acl):
        _data = {'active': True,
                'name': 'INGRESS',
                'priority_type': 'NONE',
                'priority': 100}
        if __ingress_acl.get("default") == "accept":
            _data.update(default_acction(True, 'ingress'))
        elif __ingress_acl.get('default') == 'reject':
            _data.update(default_acction(False, 'ingress'))
        else:
            print('WARNING. DEFAULT action in configuration file define incorrect, "accept" will be used')
            _data.update(default_acction(True, 'ingress'))
        push_to_vsd(domain + '_ingress_acl',
                    vsdk.NUIngressACLTemplate(data=_data),
                    vsd_obj[domain])
        # allow entries
        if __ingress_acl.get("allow"):
            for entry in create_entries("allow", __ingress_acl["allow"]):
                # if args.debug: pprint(entry)
                push_to_vsd(domain + '_ingress_acl_' + entry['description'],
                            vsdk.NUIngressACLEntryTemplate(data=entry),
                            vsd_obj[domain + '_ingress_acl'])
        else:
            print('INFO. There is no allow entries')
        # deny enties
        if __ingress_acl.get("drop"):
            for entry in create_entries("drop", __ingress_acl["drop"]):
                push_to_vsd(domain + '_ingress_acl_' + entry['description'],
                            vsdk.NUIngressACLEntryTemplate(data=entry),
                            vsd_obj[domain + '_ingress_acl'])
        else:
            print('INFO. There is no drop entries')

    def create_egress_acl(__egress_acl):
        data = {'active': True,
                'name': 'EGRESS',
                'priority_type': 'NONE',
                'priority': 100}
        if __egress_acl.get("default") == "accept":
            data.update(default_acction(True, 'egress'))
        elif __egress_acl.get('default') == 'reject':
            data.update(default_acction(False, 'egress'))
        else:
            print('WARNING. DEFAULT action in configuration file define incorrect, "accept" will be used')
            data.update(default_acction(True, 'egress'))

        push_to_vsd(domain + '_egress_acl',
                    vsdk.NUEgressACLTemplate(data=data),
                    vsd_obj[domain])

        # allow entries
        if __egress_acl.get("allow"):
            for entry in create_entries("allow", __egress_acl["allow"]):
                push_to_vsd(domain + '_ingress_acl_' + entry['description'],
                            vsdk.NUEgressACLEntryTemplate(data=entry),
                            vsd_obj[domain + '_ingress_acl'])
        else:
            print('INFO. There is no allow entries')

        # deny enties
        if __egress_acl.get("drop"):
            for entry in create_entries("drop", __egress_acl["drop"]):
                push_to_vsd(domain + '_ingress_acl_' + entry['description'],
                            vsdk.NUEgressACLEntryTemplate(data=entry),
                            vsd_obj[domain + '_ingress_acl'])
        else:
            print('INFO. There is no drop entries')

    for direction in __acl:
        print(direction)
        if direction == 'ingress':
            create_ingress_acl(__acl['ingress'])
        elif direction == 'egress':
            create_egress_acl(__acl['egress'])
        else:
            print('FAILED. Cannot identify ACL direction. Please check configuration.')
            pprint(__acl)
            exit(-1)


# Get user defined arguments
args = cli_support.get_args()

# Start logger
logger = cli_support.get_logger(args)

# Load yaml configuration file
config = cli_support.load_yaml_config(args)

# Show loaded configuration
if args.show_config:
    logger.info('Show configuration file parameters.')
    if args.verbose:
        logger.info(pformat(config))
    else:
        pprint(config)

# VSD connection
if args.init or args.add_acl:
    nc = vsdk.NUVSDSession(username=config['vsd']['username'],
                           password=config['vsd']['password'],
                           enterprise=config['vsd']['organization'],
                           api_url="https://{0}:{1}".format(config['vsd']['host'],
                                                            config['vsd']['port']))
    logger.debug('Start VSD connection')
    nc.start()
    logger.debug('VSD connection started')
    nuage_user = nc.user

    # variable to store vsd objects
    vsd_obj = {}

if args.init:
    # Provision organization, template and L3 domains overlay
    if org_already_exists(config['organization']['name']):
        logger.critical('Cannot provision network overlay. Organization already exists. Please choose another name.')
        exit(-1)
    else:
        deploy_org_networks()

if args.add_acl:
    build_vsd_obj_dict()
    if args.debug:
        pprint(vsd_obj)
        pprint(config)
    for domain in sorted(config['organization']['acls']):
        print(domain)
        push_acl_to_vsd(config['organization']['acls'][domain])
